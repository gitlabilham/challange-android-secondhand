
Feature: interested product list

  Scenario: The user want to see my interested product list
    Given the user is on the app my account page
    When the user click my product list menu
    And the user click my interested product section
    Then the user is navigated to my interested product page
