
Feature: deleteproduct
 
  
  Scenario: the user want to delete product
    Given the user click delete icon on the product that they want to delete
    When the user click confirm delete product button
    Then the user is navigated to my product list to verify product been deleted

    