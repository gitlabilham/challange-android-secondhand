Feature: My sold product


  Scenario: The user want to see my sold product list
    Given the user is navigated to the my account page
    When the user click my product list
    And the user click my sold product section
    Then the user is navigated to my sold product page
