Feature: edit product

  Scenario: The user want to edit product detail
    Given the user click the product that want to edited
    When the user edit product detail
    And the user click update product button
    Then the user is navigated to the my product list page to verify the product been edited
   