package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class terjual {

	@Given("the user is navigated to the my account page")
	def the_user_is_navigated_to_the_my_account_page() {
		Mobile.verifyElementVisible(findTestObject('Product/btn_my product list'), 0)
	}

	@When("the user click my product list")
	public void the_user_click_my_product_list() {
		Mobile.tap(findTestObject('Product/btn_my product list'), 0)
	}

	@And("the user click my sold product section")
	public void the_user_click_my_sold_product_section () {
		Mobile.tap(findTestObject('Daftar Jual Saya/btn_terjual'), 0)
	}

	@Then("the user is navigated to my sold product page")
	public void the_user_is_navigated_to_my_sold_product_page () {
		Mobile.verifyElementVisible(findTestObject('Daftar Jual Saya/btn_terjual'), 0)
		Mobile.tap(findTestObject('Daftar Jual Saya/btn_kembali daftar jual saya'), 0)
	}
}