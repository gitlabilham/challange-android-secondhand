package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class addproduct {

	@Given("the user click + icon button")
	def the_user_click_add_product_icon() {
		Mobile.tap(findTestObject('Product/btn_add product'), 0)
	}

	@When("the user input product detail")
	public void the_user_input_product_detail() {
		Mobile.setText(findTestObject('Product/txt_product name'), 'Sarung Tangan Bola lloris', 0)
		Mobile.setText(findTestObject('Product/txt_product price'), '200000', 0)
		Mobile.tap(findTestObject('Product/choosing_product category'), 0, FailureHandling.STOP_ON_FAILURE)
		Mobile.tap(findTestObject('Product/category_Hobi dan Koleksi'), 0)
		Mobile.setText(findTestObject('Product/txt_product location'), 'Jakarta', 0)
		Mobile.hideKeyboard()
		Mobile.setText(findTestObject('Product/txt_product desc'), 'Sarung tangan bola bekas lloris', 0)
		Mobile.hideKeyboard()
		Mobile.tap(findTestObject('Product/add_product img'), 0)
		Mobile.tap(findTestObject('Product/btn_product img from gallery'), 0)
		Mobile.tap(findTestObject('Product/btn_choosing product img from galalery'), 0)
		Mobile.hideKeyboard()
	}

	@And("the user click publish button")
	public void the_user_click_publish_button() {
		Mobile.tap(findTestObject('Product/btn_terbitkan'), 0)
	}

	@Then("the user is navigated to the my product list page to verify the product been added")
	public void the_user_is_verify_product() {
		Mobile.verifyElementVisible(findTestObject('Product/verify_my product list'), 0)
		Mobile.tap(findTestObject('Daftar Jual Saya/btn_kembali daftar jual saya'), 0)
	}
}