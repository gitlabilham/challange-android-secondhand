package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class loginstep {
	@Given("user open app mobile")
	def navigateToLoginPage() {
		Mobile.startApplication('C:\\Users\\wrkng\\Desktop\\Platinum challange\\android\\challange-android-secondhand\\APK\\secondhand-01022024.apk',
				false)
		Mobile.tap(findTestObject('Registrasi Akun/00. Logo Akun'), 0)
		Mobile.tap(findTestObject('Registrasi Akun/01. Tombol Masuk'), 0)
	}

	@When("user enter username and password")
	public void user_enter_username_and_password() {
		Mobile.setText(findTestObject('Login/02. Input Email Login'), 'leo_kel3@binar.com', 0)
		Mobile.setText(findTestObject('Login/03. Input Password Login'), '123456', 0)
		Mobile.tap(findTestObject('Login/04. Tombol Masuk'), 0)
	}


	@Then("user open login mobile")
	public void user_is_navigated_on_homepage() {
		Mobile.verifyElementVisible(findTestObject('Login/05. Verfiy Akun Saya'), 0)
	}
}