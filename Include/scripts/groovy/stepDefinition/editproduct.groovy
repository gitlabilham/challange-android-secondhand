package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class editproduct {

	@Given("the user click the product that want to edited")
	def the_user_click_the_product_that_want_to_edit() {
		Mobile.tap(findTestObject('Product/btn_edit product'), 0)
	}

	@When("the user edit product detail")
	public void the_user_edit_product_detail() {
		Mobile.setText(findTestObject('Product/Edit Produk/txt_EditText Nama Produk'), 'Sarung Tangan Bola Erspo Versi 2', 0)
		Mobile.hideKeyboard()
		Mobile.verifyElementVisible(findTestObject('Product/Edit Produk/txt_LinearLayout Harga Produk'), 0, FailureHandling.STOP_ON_FAILURE)
		Mobile.verifyElementVisible(findTestObject('Product/btn_edit product category'), 0)
		Mobile.verifyElementVisible(findTestObject('Product/Edit Produk/txt_LinearLayout Lokasi Produk'), 0, FailureHandling.STOP_ON_FAILURE)
		Mobile.verifyElementVisible(findTestObject('Product/Edit Produk/txt_LinearLayout Deskripsi Produk'), 0, FailureHandling.STOP_ON_FAILURE)
		Mobile.verifyElementVisible(findTestObject('Product/btn_edit product img'), 0)
	}

	@And("the user click update product button")
	public void the_user_click_update_product_button() {
		Mobile.tap(findTestObject('Product/btn_update product'), 0)
	}

	@Then("the user is navigated to the my product list page to verify the product been edited")
	public void the_user_is_verify_edited_product() {
		Mobile.verifyElementVisible(findTestObject('Product/verify_my product list'), 0)
		Mobile.tap(findTestObject('Daftar Jual Saya/btn_kembali daftar jual saya'), 0)
	}
}