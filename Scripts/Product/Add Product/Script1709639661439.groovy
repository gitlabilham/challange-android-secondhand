import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication(GlobalVariable.url_ilham, false)

not_run: Mobile.tap(findTestObject('Registrasi Akun/00. Logo Akun'), 0)

not_run: Mobile.tap(findTestObject('Registrasi Akun/01. Tombol Masuk'), 0)

not_run: Mobile.setText(findTestObject('Login/02. Input Email Login'), 'leo_kel3@binar.com', 0)

not_run: Mobile.setText(findTestObject('Login/03. Input Password Login'), '123456', 0)

not_run: Mobile.tap(findTestObject('Login/04. Tombol Masuk'), 0)

not_run: Mobile.verifyElementVisible(findTestObject('Login/05. Verfiy Akun Saya'), 0)

Mobile.tap(findTestObject('Product/btn_add product'), 0)

Mobile.setText(findTestObject('Product/txt_product name'), 'Sarung Tangan Bola', 0)

Mobile.setText(findTestObject('Product/txt_product price'), '150000', 0)

Mobile.tap(findTestObject('Product/choosing_product category'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Product/category_Hobi dan Koleksi'), 0)

Mobile.setText(findTestObject('Product/txt_product location'), 'Jakarta', 0)

Mobile.hideKeyboard()

Mobile.setText(findTestObject('Product/txt_product desc'), 'Sarung tangan bola bekas André Onana', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Product/add_product img'), 0)

Mobile.tap(findTestObject('Product/btn_product img from gallery'), 0)

Mobile.tap(findTestObject('Product/btn_choosing product img from galalery'), 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Product/btn_terbitkan'), 0)

Mobile.verifyElementVisible(findTestObject('Product/verify_my product list'), 0)

