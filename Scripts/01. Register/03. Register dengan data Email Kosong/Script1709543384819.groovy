import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Tugas Platinum\\Mobile\\secondhand-01022024.apk', false)

Mobile.tap(findTestObject('Registrasi Akun/00. Logo Akun'), 0)

Mobile.tap(findTestObject('Registrasi Akun/01. Tombol Masuk'), 0)

Mobile.tap(findTestObject('Registrasi Akun/02. Txt Daftar'), 0)

Mobile.setText(findTestObject('Registrasi Akun/03. Input Nama Lengkap'), 'Leo Maret 2024', 1)

Mobile.setText(findTestObject('Registrasi Akun/04. Input Email'), '', 0)

Mobile.setText(findTestObject('Registrasi Akun/05. Input Password'), '123456', 0)

Mobile.setText(findTestObject('Registrasi Akun/06. Input No HP'), '0813 0813 0813', 0)

Mobile.setText(findTestObject('Registrasi Akun/07. Input Kota'), 'Medan', 0)

Mobile.setText(findTestObject('Registrasi Akun/08. Input Alamat'), 'Jalan Kelompok No. 3', 0)

Mobile.tap(findTestObject('Registrasi Akun/09. Tombol Daftar'), 0)

Mobile.verifyElementVisible(findTestObject('Registrasi Akun/11. Vefiry - Email tidak boleh kosong'), 0)

