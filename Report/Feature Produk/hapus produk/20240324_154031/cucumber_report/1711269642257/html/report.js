$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/android/challange-android-secondhand/Include/features/login.feature");
formatter.feature({
  "name": "Login Feature",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to login with data valid",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user open app mobile",
  "keyword": "Given "
});
formatter.match({
  "location": "loginstep.navigateToLoginPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user enter username and password",
  "keyword": "When "
});
formatter.match({
  "location": "loginstep.user_enter_username_and_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user open login mobile",
  "keyword": "Then "
});
formatter.match({
  "location": "loginstep.user_is_navigated_on_homepage()"
});
formatter.result({
  "status": "passed"
});
});