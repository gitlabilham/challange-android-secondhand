$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/android/challange-android-secondhand/Include/features/deleteproduct.feature");
formatter.feature({
  "name": "deleteproduct",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "the user want to delete product",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the user click delete icon on the product that they want to delete",
  "keyword": "Given "
});
formatter.match({
  "location": "deleteproduct.the_user_click_delete_product_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click confirm delete product button",
  "keyword": "When "
});
formatter.match({
  "location": "deleteproduct.the_user_confirm_delete()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user is navigated to my product list to verify product been deleted",
  "keyword": "Then "
});
formatter.match({
  "location": "deleteproduct.the_user_verify_delete_product()"
});
formatter.result({
  "status": "passed"
});
});