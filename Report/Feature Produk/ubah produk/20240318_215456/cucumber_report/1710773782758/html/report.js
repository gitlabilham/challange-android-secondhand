$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/android/challange-android-secondhand/Include/features/editproduct.feature");
formatter.feature({
  "name": "edit product",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "The user want to edit product detail",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the user click the product that want to edited",
  "keyword": "Given "
});
formatter.match({
  "location": "editproduct.the_user_click_the_product_that_want_to_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user edit product detail",
  "keyword": "When "
});
formatter.match({
  "location": "editproduct.the_user_edit_product_detail()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click update product button",
  "keyword": "And "
});
formatter.match({
  "location": "editproduct.the_user_click_update_product_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user is navigated to the my product list page to verify the product been edited",
  "keyword": "Then "
});
formatter.match({
  "location": "editproduct.the_user_is_verify_edited_product()"
});
formatter.result({
  "status": "passed"
});
});