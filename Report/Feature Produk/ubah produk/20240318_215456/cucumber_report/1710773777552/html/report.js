$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/android/challange-android-secondhand/Include/features/produksaya.feature");
formatter.feature({
  "name": "My Product List",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "The user want to see my product list",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the user is on my account page",
  "keyword": "Given "
});
formatter.match({
  "location": "produksaya.the_user_is_on_my_account_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click my product list button",
  "keyword": "When "
});
formatter.match({
  "location": "produksaya.the_user_click_my_product_list_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user is navigated to my product list app page",
  "keyword": "Then "
});
formatter.match({
  "location": "produksaya.the_user_is_navigated_to_my_product_list_app_page()"
});
formatter.result({
  "status": "passed"
});
});