$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/android/challange-android-secondhand/Include/features/addproduct.feature");
formatter.feature({
  "name": "addproduct",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "the user want to add product to sell",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the user click + icon button",
  "keyword": "Given "
});
formatter.match({
  "location": "addproduct.the_user_click_add_product_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user input product detail",
  "keyword": "When "
});
formatter.match({
  "location": "addproduct.the_user_input_product_detail()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click publish button",
  "keyword": "And "
});
formatter.match({
  "location": "addproduct.the_user_click_publish_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user is navigated to the my product list page to verify the product been added",
  "keyword": "Then "
});
formatter.match({
  "location": "addproduct.the_user_is_verify_product()"
});
formatter.result({
  "status": "passed"
});
});