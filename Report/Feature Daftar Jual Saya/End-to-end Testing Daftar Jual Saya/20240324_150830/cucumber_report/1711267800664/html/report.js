$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/android/challange-android-secondhand/Include/features/terjual.feature");
formatter.feature({
  "name": "My sold product",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "The user want to see my sold product list",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the user is navigated to the my account page",
  "keyword": "Given "
});
formatter.match({
  "location": "terjual.the_user_is_navigated_to_the_my_account_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click my product list",
  "keyword": "When "
});
formatter.match({
  "location": "terjual.the_user_click_my_product_list()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click my sold product section",
  "keyword": "And "
});
formatter.match({
  "location": "terjual.the_user_click_my_sold_product_section()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user is navigated to my sold product page",
  "keyword": "Then "
});
formatter.match({
  "location": "terjual.the_user_is_navigated_to_my_sold_product_page()"
});
formatter.result({
  "status": "passed"
});
});