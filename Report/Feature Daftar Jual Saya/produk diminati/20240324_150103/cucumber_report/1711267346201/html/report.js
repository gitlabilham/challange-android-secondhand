$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/android/challange-android-secondhand/Include/features/diminati.feature");
formatter.feature({
  "name": "interested product list",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "The user want to see my interested product list",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the user is on the app my account page",
  "keyword": "Given "
});
formatter.match({
  "location": "diminati.the_user_is_navigated_to_themyaccountpage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click my product list menu",
  "keyword": "When "
});
formatter.match({
  "location": "diminati.the_user_click_myproductlist()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click my interested product section",
  "keyword": "And "
});
formatter.match({
  "location": "diminati.the_user_click_my_intersted_product_section()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user is navigated to my interested product page",
  "keyword": "Then "
});
formatter.match({
  "location": "diminati.the_user_is_navigated_to_my_interested_product_page()"
});
formatter.result({
  "status": "passed"
});
});